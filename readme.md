# WordPress Plugin - Advanced Custom Field: Searchable

Enable searchable toggle on individual field.

[ ![Bitbucket Pipeline Status for gummi-acf/acf-searchable](https://bitbucket-badges.atlassian.io/badge/gummi-acf/acf-searchable.svg)](https://bitbucket.org/gummi-acf/acf-searchable/overview)
