<?php
namespace Gummiforweb\AcfSearchable;

class AcfSearchableOption
{
    public function __construct()
    {
        add_action('acf/field_group/admin_enqueue_scripts', [$this, 'enqueyeFieldGroupScripts']);
        add_action('acf/render_field_settings', [$this, 'maybeEnableSearchableSetting'], 20);
        add_action('acf/update_field', [$this, 'updateSearchableSetting'], 20);
    }

    public function enqueyeFieldGroupScripts()
    {
        $min = defined('SCRIPT_DEBUG') && SCRIPT_DEBUG? 'min.' : '';
        $jsFile = "/assets/js/acf-searchable-field-group.{$min}js";
        $cssFile = "/assets/css/acf-searchable-field-group.{$min}css";

        wp_enqueue_script(
            'acf-searchable-field-group',
            ACF_SEARCHABLE_URL . $jsFile,
            ['acf-field-group'],
            filemtime(ACF_SEARCHABLE_PATH . $jsFile)
        );

        wp_enqueue_style(
            'acf-searchable-field-group',
            ACF_SEARCHABLE_URL . $cssFile,
            ['acf-field-group'],
            filemtime(ACF_SEARCHABLE_PATH . $cssFile)
        );
    }

    public function maybeEnableSearchableSetting($field)
    {
        $enabled = acf_searchable_is_field_type_enabled($field['type']);
        $function = $enabled? 'add_action' : 'remove_action';

        $function("acf/render_field_settings/type={$field['type']}", [$this, 'appendSearchableSetting'], 20);
    }

    public function updateSearchableSetting($field)
    {
        if (array_key_exists('acf_searchable', $field)) return $field;
        if (! acf_searchable_is_field_type_enabled($field['type'])) return $field;

        $field['acf_searchable'] = acf_searchable_is_field_type_searchable($field['type']);

        return $field;
    }

    public function appendSearchableSetting($field)
    {
        acf_render_field_setting($field, [
            'label'         => __('Searchable', 'acf-searchable'),
            'instructions'  => __('Make WP_Query\'s default search includes this field', 'acf-searchable'),
            'name'          => 'acf_searchable',
            'type'          => 'true_false',
            'ui'            => true,
            'default_value' => acf_searchable_is_field_type_searchable($field['type']),
        ]);
    }

}
