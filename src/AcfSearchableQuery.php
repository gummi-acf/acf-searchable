<?php
namespace Gummiforweb\AcfSearchable;

class AcfSearchableQuery
{
    public function __construct()
    {
        add_filter('posts_distinct', [$this, 'addDistinct'], 15, 2);
        add_filter('posts_join', [$this, 'addJoinTables'], 15, 2);
        add_filter('posts_search', [$this, 'modifySearchClause'], 15, 2);
        add_filter('posts_search_orderby', [$this, 'modifySearchOrderbyClause'], 15, 2);
    }

    public function addDistinct($distinct, $query)
    {
        if (! $this->needToPerformSearch($query)) return $distinct;

        return 'DISTINCT';
    }

    public function addJoinTables($join, $query)
    {
        if (! $this->needToPerformSearch($query)) return $join;

        $join .= $this->prepareAcfFieldSearchJoins();

        return $join;
    }

    public function modifySearchClause($search, $query)
    {
        if (! $this->needToPerformSearch($query)) return $search;

        $search = $this->revertSearchClause($search);
        $acfSearch = $this->prepareAcfSerachClause($query);

        return $this->unrevertSearchClause("$search OR $acfSearch");
    }

    public function modifySearchOrderbyClause($orderBy, $query)
    {
        if (! $this->needToPerformSearch($query)) return $orderBy;

        $acfOrderBy = $this->prepareAcfSearchOrderClause($query);

        return str_replace(' ELSE 6 END', " {$acfOrderBy} ELSE 6 END", $orderBy);
    }

    protected function prepareAcfFieldSearchJoins()
    {
        global $wpdb;

        $joins = [];

        // join 2 tables for metas (field_name and field_value)
        $joins[] = "LEFT JOIN {$wpdb->postmeta} AS acf_ref ON acf_ref.post_id = {$wpdb->posts}.ID";
        $joins[] = "LEFT JOIN {$wpdb->postmeta} AS acf_value ON acf_value.post_id = {$wpdb->posts}.ID";

        // join 1 table for actual field post, to check against field_name
        $on = implode(' OR ', array_map([$this, 'prepareFieldTypeJoinOn'], acf_searchable_get_enabled_field_types()));
        $joins[] = "LEFT JOIN {$wpdb->posts} AS acf_field ON ({$on}) AND acf_field.post_type = 'acf-field'";

        return implode(' ' , $joins);
    }

    protected function prepareFieldTypeJoinOn($fieldType)
    {
        $typeMatch = sprintf('s:4:"type";s:%d:"%s";', strlen($fieldType), $fieldType);
        $searchableTrue = '%s:14:"acf_searchable";%:1;%'; // b:1 or i:1
        $searchableIsset = '%s:14:"acf_searchable";%';

        $on = "(acf_field.post_content LIKE '%{$typeMatch}%' AND acf_field.post_content LIKE '{$searchableTrue}')";

        if (acf_searchable_is_field_type_searchable($fieldType)) {
            $on .= " OR (acf_field.post_content LIKE '%{$typeMatch}%' AND acf_field.post_content NOT LIKE '{$searchableIsset}')";
        }

        return $on;
    }

    protected function prepareAcfSerachClause($query)
    {
        global $wpdb;

        $search = [];
        $n = $query->get('exact')? '' : '%';
        $exclusionPrefix = apply_filters('wp_query_search_exclusion_prefix', '-');
        $table = 'acf_value.meta_key = SUBSTRING(acf_ref.meta_key, 2) AND acf_field.post_name = acf_ref.meta_value';

        if ($localSearchableFields = $this->getLocalSearchableFields()) {
            $localSearchableFieldKeys = "'" . implode("', '",  wp_list_pluck($localSearchableFields, 'key')) . "'";
            $localTable = "acf_value.meta_key = SUBSTRING(acf_ref.meta_key, 2) AND acf_ref.meta_value IN({$localSearchableFieldKeys})";
            $table = "(({$table}) OR ({$localTable}))";
        }

        foreach ($query->get('search_terms') as $term) {
            if ($exclude = $exclusionPrefix && ($exclusionPrefix === substr($term, 0, 1))) {
                $term = substr($term, 1);
            }

            $like_op = $exclude? 'NOT LIKE' : 'LIKE';
            $like = $n . $wpdb->esc_like($term) . $n;
            $search[] = $wpdb->prepare("(acf_value.meta_value $like_op %s AND $table)", $like);
        }

        $search = implode(' OR ', $search);
        // $acfMetaClause = '(acf_value.meta_key = SUBSTRING(acf_ref.meta_key, 2)) AND (acf_field.post_name = acf_ref.meta_value)';

        return "({$search})";
        // return "(({$search}) AND {$acfMetaClause})";
    }

    protected function revertSearchClause($search)
    {
        global $wpdb;

        if ( !is_user_logged_in()) {
            // class-wp-query.php:1313
            $search = str_replace(" AND ({$wpdb->posts}.post_password = '') ", '', $search);
        }

        return substr($search, 6, -2); // " AND ({$search}) ";
    }

    protected function unrevertSearchClause($search)
    {
        global $wpdb;

        $search = " AND ({$search}) ";

        if (! is_user_logged_in()) {
            $search .= " AND ({$wpdb->posts}.post_password = '') ";
        }

        return $search;
    }

    protected function prepareAcfSearchOrderClause($query)
    {
        global $wpdb;

        $orderBy = '';
        $searchOrderByTitle = array_map(function($sql) use ($wpdb) {
            return str_replace("{$wpdb->posts}.post_title", 'acf_value.meta_value', $sql);
        }, $query->get('search_orderby_title'));

        if ($query->get('search_terms_count') > 1) {
            $numTerms = count($query->get('search_orderby_title'));

            $like = '';
            if (! preg_match('/(?:\s|^)\-/', $query->get('s'))) {
                $like = '%' . $wpdb->esc_like($query->get('s')) . '%';
            }

            if ($like) {
                $orderBy .= $wpdb->prepare("WHEN acf_value.meta_value LIKE %s THEN 4 ", $like);
            }

            if ($numTerms < 7) {
                $orderBy .= 'WHEN ' . implode(' AND ', $searchOrderByTitle) . ' THEN 5 ';

                // if ($numTerms > 1) {
                //     $orderBy .= 'WHEN ' . implode(' OR ', $searchOrderByTitle) . ' THEN 5 ';
                // }
            }
        } else {
            $orderBy = reset($searchOrderByTitle) . ' DESC';
        }

        return $orderBy;
    }

    protected function getLocalSearchableFields()
    {
        return array_filter(acf_local()->fields, function($field) {
            if (! acf_searchable_is_field_type_enabled($field['type'])) return false;

            return isset($field['acf_searchable'])?
                $field['acf_searchable'] :
                acf_searchable_is_field_type_searchable($field['type']);
        });
    }

    protected function needToPerformSearch($query)
    {
        if (! apply_filters('acf-searchable/auto_apply_to_wp_query', true)) return false;
        if ($query->get('disable_acf_search')) return false;
        if (! acf_searchable_get_enabled_field_types()) return false;

        return !! $query->get('s');
    }
}
