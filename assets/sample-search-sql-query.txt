SELECT DISTINCT wptests_posts.*
FROM   wptests_posts
       LEFT JOIN wptests_postmeta AS acf_ref
              ON acf_ref.post_id = wptests_posts.id
       LEFT JOIN wptests_postmeta AS acf_value
              ON acf_value.post_id = wptests_posts.id
       LEFT JOIN wptests_posts AS acf_field
              ON ( ( acf_field.post_content LIKE '%s:4:"type";s:6:"select";%'
                     AND acf_field.post_content LIKE
                         '%s:14:"acf_searchable";%:1;%' )
                    OR ( acf_field.post_content LIKE '%s:4:"type";s:4:"text";%'
                         AND acf_field.post_content LIKE
                             '%s:14:"acf_searchable";%:1;%'
                       )
                    OR ( acf_field.post_content LIKE '%s:4:"type";s:4:"text";%'
                         AND acf_field.post_content NOT LIKE
                             '%s:14:"acf_searchable";%'
                       )
                    OR ( acf_field.post_content LIKE
                         '%s:4:"type";s:8:"textarea";%'
                         AND acf_field.post_content LIKE
                             '%s:14:"acf_searchable";%:1;%'
                       )
                    OR ( acf_field.post_content LIKE
                         '%s:4:"type";s:8:"textarea";%'
                         AND acf_field.post_content NOT LIKE
                             '%s:14:"acf_searchable";%'
                       )
                    OR ( acf_field.post_content LIKE
                         '%s:4:"type";s:7:"wysiwyg";%'
                         AND acf_field.post_content LIKE
                             '%s:14:"acf_searchable";%:1;%'
                       )
                    OR ( acf_field.post_content LIKE
                         '%s:4:"type";s:7:"wysiwyg";%'
                         AND acf_field.post_content NOT LIKE
                             '%s:14:"acf_searchable";%'
                       ) )
                 AND acf_field.post_type = 'acf-field'
WHERE  1 = 1
       AND ( ( ( wptests_posts.post_title LIKE '%fox%' )
                OR ( wptests_posts.post_excerpt LIKE '%fox%' )
                OR ( wptests_posts.post_content LIKE '%fox%' ) )
             AND ( ( wptests_posts.post_title LIKE '%jumps%' )
                    OR ( wptests_posts.post_excerpt LIKE '%jumps%' )
                    OR ( wptests_posts.post_content LIKE '%jumps%' ) )
              OR ( ( acf_value.meta_value LIKE '%fox%'
                     AND ( ( acf_value.meta_key = Substring(acf_ref.meta_key, 2)
                             AND acf_field.post_name = acf_ref.meta_value )
                            OR ( acf_value.meta_key =
                                 Substring(acf_ref.meta_key, 2)
                                 AND acf_ref.meta_value IN(
                                     'field_59c8b8ffdf173' ) ) )
                   )
                    OR ( acf_value.meta_value LIKE '%jumps%'
                         AND ( ( acf_value.meta_key =
                                 Substring(acf_ref.meta_key, 2)
                                 AND acf_field.post_name = acf_ref.meta_value )
                                OR ( acf_value.meta_key =
                                     Substring(acf_ref.meta_key, 2
                                     )
                                     AND acf_ref.meta_value IN(
                                         'field_59c8b8ffdf173' )
                                   ) )
                       )
                 ) )
       AND ( wptests_posts.post_password = '' )
       AND wptests_posts.post_type IN ( 'post', 'page', 'attachment' )
       AND ( wptests_posts.post_status = 'publish'
              OR wptests_posts.post_status = 'acf-disabled' )
ORDER  BY ( CASE
              WHEN wptests_posts.post_title LIKE '%fox jumps%' THEN 1
              WHEN wptests_posts.post_title LIKE '%fox%'
                   AND wptests_posts.post_title LIKE '%jumps%' THEN 2
              WHEN wptests_posts.post_title LIKE '%fox%'
                    OR wptests_posts.post_title LIKE '%jumps%' THEN 3
              WHEN wptests_posts.post_excerpt LIKE '%fox jumps%' THEN 4
              WHEN wptests_posts.post_content LIKE '%fox jumps%' THEN 5
              WHEN acf_value.meta_value LIKE '%fox jumps%' THEN 4
              WHEN acf_value.meta_value LIKE '%fox%'
                   AND acf_value.meta_value LIKE '%jumps%' THEN 5
              ELSE 6
            end ),
          wptests_posts.post_date DESC
