<?php

namespace Tests\Feature;

use Tests\TestCase;

class AcfSearchableOptionTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();

        $this->searchableAction = [$GLOBALS['acf_searchable_option'], 'appendSearchableSetting'];
    }

    /** @test */
    public function it_will_only_append_searchable_field_to_enabled_field_type()
    {
        $field = $this->textField = $this->acfDbField('text', 'text_field');
        do_action('acf/render_field_settings', $field);
        $this->assertTrue(!! has_action('acf/render_field_settings/type=text', $this->searchableAction));

        $field = $this->textField = $this->acfDbField('radio', 'text_field');
        do_action('acf/render_field_settings', $field);
        $this->assertFalse(!! has_action('acf/render_field_settings/type=radio', $this->searchableAction));
    }

    /** @test */
    public function searchable_field_can_be_allowed_on_run_time()
    {
        $field = $this->textField = $this->acfDbField('text', 'text_field');

        do_action('acf/render_field_settings', $field);
        $this->assertTrue(!! has_action('acf/render_field_settings/type=text', $this->searchableAction));

        add_filter('acf-searchable/enabled_field_types', function() { return ['textarea']; });

        do_action('acf/render_field_settings', $field);
        $this->assertFalse(!! has_action('acf/render_field_settings/type=text', $this->searchableAction));
    }

    /** @test */
    public function field_created_by_php_should_containct_searchable_setting_if_field_type_is_enabled()
    {
        $field = $this->acfDbField('text', 'text_field');
        $field2 = $this->acfDbField('radio', 'radio_field');

        $this->assertArrayHasKey('acf_searchable', $field);
        $this->assertArrayNotHasKey('acf_searchable', $field2);
    }

    /** @test */
    public function fields_created_by_php_should_have_proper_default_value()
    {
        $field = $this->acfDbField('text', 'text_field');
        $field2 = $this->acfDbField('select', 'select_field');

        $this->assertTrue($field['acf_searchable']);
        $this->assertFalse($field2['acf_searchable']);
    }
}
