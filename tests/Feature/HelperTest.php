<?php

namespace Tests\Feature;

use Tests\TestCase;

class HelperTest extends TestCase
{
    /** @test */
    public function field_type_can_be_enabled_by_filter()
    {
        add_filter('acf-searchable/enabled_field_types', function($fieldTypes) {
            $fieldTypes[] = 'image';
            return $fieldTypes;
        });

        $this->assertContains('image', acf_searchable_get_enabled_field_types());
    }

    /** @test */
    public function invalid_field_type_will_not_be_returned()
    {
        add_filter('acf-searchable/enabled_field_types', function($fieldTypes) {
            $fieldTypes[] = 'i-dont-exists';
            return $fieldTypes;
        });

        $this->assertNotContains('i-dont-exists', acf_searchable_get_enabled_field_types());
    }

    /** @test */
    public function field_type_searchable_default_can_be_set_by_filter()
    {
        add_filter('acf-searchable/default_searchable_field_types', function($fieldTypes) {
            $fieldTypes[] = 'image';
            return $fieldTypes;
        });

        $this->assertContains('image', acf_searchable_get_default_searchable_field_types());
    }

    /** @test */
    public function invalid_field_type_will_not_be_set()
    {
        add_filter('acf-searchable/default_searchable_field_types', function($fieldTypes) {
            $fieldTypes[] = 'i-dont-exists';
            return $fieldTypes;
        });

        $this->assertNotContains('i-dont-exists', acf_searchable_get_default_searchable_field_types());
    }

    /** @test */
    public function it_will_determine_if_a_field_type_is_enabled()
    {
        $this->assertTrue(acf_searchable_is_field_type_enabled('text'));
        $this->assertFalse(acf_searchable_is_field_type_enabled('image'));
    }

    /** @test */
    public function it_will_return_if_field_type_is_searchable_by_default()
    {
        $this->assertTrue(acf_searchable_is_field_type_searchable('text'));
        $this->assertFalse(acf_searchable_is_field_type_searchable('image'));
    }
}
